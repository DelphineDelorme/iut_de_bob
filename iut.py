def fonction():
    return True

def not_fonction():
    return False


def test_fonction():
    assert fonction()
    assert not not_fonction()

def dunder():
    return "__"

def not_dunder():
    return "--"

def test_dunder():
    assert dunder() == "__"
    assert not_dunder() == "--"

def dory():
    return "Heloise"

def not_dory():
    return "Pas Heloise :("

def test_dory():
    assert dory() == "Heloise"
    assert not dory() == not_dory()

def cookies():
    return "C'est la vie"

def not_cookies():
    return "Non, toujours COOKIES"

def test_des_COOKIES():
    assert cookies() == "C'est la vie"
    assert not_cookies() == "Non, toujours COOKIES"

def ai_je_faim():
    return True

def suis_je_motivee():
    return False

def test_faim():
    assert ai_je_faim()

def test_motivation():
    assert not suis_je_motivee()
